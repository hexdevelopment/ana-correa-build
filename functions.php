<?php

// Removal of un-needed WordPress functions/styles/scripts which slow it down.
require_once('functions/clean_up.php');

// Removal of un-needed WordPress admin functions.
require_once('functions/admin_tidyup.php');

// Setup of CSS/JS files being pulled into FED.
require_once('functions/enqueued_scripts.php');

// Setup of simple WordPress navs.
require_once('functions/nav.php');

// Simple funcutions for pulling time, category and tags.
require_once('functions/post_functions.php');

// Added option for excerpt length and ending.
require_once('functions/excerpts.php');

// Function for creating related posts section.
require_once('functions/related_posts.php');

// Setup file for Custom Post Types to be dropped into.
require_once('functions/custom_post_types.php');

// Setup of ACF Options page with general re-useable fields.
require_once('functions/acf_options.php');