jQuery(document).ready(function($){ 
	
	// Mobile Button
	$('.menu-icon').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('open');
		$('nav.mobile').slideToggle();
	});


	$('.testimonials-slider').owlCarousel({
		loop:true,
		items:1,
		center:true,
		nav:true,
		navText:["<i class='fas fa-caret-left fa-4x purple'></i>","<i class='fas fa-caret-right fa-4x purple'></i>"]
	});
	//new WOW().init();
	
});

jQuery(window).load(function($) {

});