		<footer>
			<div class="container">
				<div class="grid">
					<div class="col-md-4 col-sm-12">
						<div class="footer-menu">
							<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => '', ) ); ?>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="addresses">
							<p><?php the_field( 'address_line_1', 'option' ); ?></p>
							<p><?php the_field( 'address_line_2', 'option' ); ?></p>
							<p><?php the_field( 'address_line_3', 'option' ); ?></p>
							<p><?php the_field( 'address_line_4', 'option' ); ?></p>
							<p><?php the_field( 'address_line_5', 'option' ); ?></p>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="contacts">
							<h5>Landline</h5>
							<p><?php the_field( 'landline', 'option' ); ?></p>
							<h5>Mobile</h5>
							<p><?php the_field( 'mobile', 'option' ); ?></p>
							<h5>Email</h5>
							<p><?php the_field( 'email', 'option' ); ?></p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>

	</body>

</html>