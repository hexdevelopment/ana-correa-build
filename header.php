<!doctype html>
	<html <?php language_attributes(); ?>>
		<head>
			<link rel="dns-prefetch" href="//fonts.googleapis.com">
			<link rel="dns-prefetch" href="//google-analytics.com">

			<meta charset="<?php bloginfo('charset'); ?>">
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="apple-mobile-web-app-capable" content="yes"/>

			<title><?php wp_title(''); ?></title>
			
			<meta name="author" content="<?php bloginfo('url'); ?> - <?php bloginfo('name'); ?>" />
			<meta name="copyright" content="Copyright <?php bloginfo('name'); ?> <?php echo date("Y"); ?>, All Rights Reserved." />
			<meta name="application-name" content="<?php bloginfo('name'); ?>"/>
			
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
			<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
			<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
			<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
			<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">

			<?php wp_head(); ?>

		</head>
		
		<body <?php body_class(); ?>>

			<nav class="mobile">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => '', ) ); ?>
			</nav>
		
			<header>
				<div class="container">
					<div class="grid">
						<div class="col-3">
							<div class="logo">
								<?php $logo = get_field( 'logo', 'option' ); ?>
								<?php if ( $logo ) { ?>
									<a href="<?php echo get_site_url(); ?>">
										<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
									</a>
								<?php } ?>
							</div>
						</div>
						<div class="col-9">
							<div class="header-contents">
								<nav>
									<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => '', ) ); ?>
								</nav>
								<a href="#" class="menu-icon"><span></span><span></span><span></span><span></span></a>
							</div>
						</div>
					</div>
				</div>
			</header>