<?php if ( have_rows( 'panels' ) ): ?>
	<?php while ( have_rows( 'panels' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'hero' ) : ?>
			<section class="container-fluid hero">
				<div class="hero-wrapper">
					<h1 class="hero-title"><?php the_sub_field( 'hero_title' ); ?></h1>
				</div>
			</section>	
		<?php elseif ( get_row_layout() == 'two_col' ) : ?>
			<section class="two-col padding">
				<div class="container-fluid">
					<div class="grid">
						<div class="col-md-6 col-sm-12 column-1">
							<?php $col_image = get_sub_field( 'col_image' ); ?>
							<?php if ( $col_image ) { ?>
								<img src="<?php echo $col_image['url']; ?>" alt="<?php echo $col_image['alt']; ?>" />
							<?php } ?>
						</div>
						<div class="col-md-6 col-sm-12 column-2">
							<h2 class="column-title"><?php the_sub_field( 'col_title' ); ?></h2>
							<?php the_sub_field( 'col_content' ); ?>
							<a class="column-button" href="<?php the_sub_field( 'col_link_link' ); ?>"><?php the_sub_field( 'col_link_text' ); ?></a>
						</div>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'squares' ) : ?>
			<section class="squares padding">
				<div class="container">
					<div class="grid">
						<?php if ( have_rows( 'squares' ) ) : ?>
							<?php while ( have_rows( 'squares' ) ) : the_row(); ?>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="square">
										<div class="content">
											<div class="square-img">	
												<?php $square_image = get_sub_field( 'square_image' ); ?>
												<?php if ( $square_image ) { ?>
													<img src="<?php echo $square_image['url']; ?>" alt="<?php echo $square_image['alt']; ?>" />
												<?php } ?>
											</div>
											<a class="square-button" href="<?php the_sub_field( 'square_button_link' ); ?>"><?php the_sub_field( 'square_button_text' ); ?></a>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'video' ) : ?>
			<section class="video padding-large">
				<div class="container">
					<div class="grid">
						<div class="col-md-12">
							<div class="video-wrapper">
								<iframe class="video_frame" src="<?php the_sub_field('video_link');?>" frameborder="0" allow="accelerometer;z encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'testimonials_slider' ) : ?>
			<section class="testimonials-slides padding">
				<div class="container">
					<div class="testimonials-slider owl-carousel">
						<?php while(have_rows('testimonials')) : the_row(); ?>
							<div class="item">
								<?php the_sub_field( 'content' ); ?>
								<h4><?php the_sub_field( 'name' ); ?>, <?php the_sub_field( 'loaction' ); ?></h4>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'cta_panel' ) : ?>
			<section class="cta-panel padding-top">
				<div class="container">
					<div class="cta-inner">
						<h4><?php the_sub_field( 'cta_panel_text' ); ?></h4>
						<a class="cta-button" href="<?php the_sub_field( 'cta_button_link' ); ?>"><?php the_sub_field( 'cta_button_text' ); ?></a>
					</div>
				</div>
			</section>
		<?php elseif ( get_row_layout() == 'testimonials' ) : ?>
			<?php $background_image = get_sub_field( 'background_image' ); ?>
			<?php if ( $background_image ) { ?>
				<section class="testimonials padding" style="background-image: url(<?php echo $background_image['url']; ?>);">
					<div class="container">
						<div class="row-header">
							<h2><?php the_title(); ?></h2>
						</div>
						<div class="grid">
							<?php if ( have_rows( 'testimonials' ) ) : ?>
								<?php while ( have_rows( 'testimonials' ) ) : the_row(); ?>
									<div class="col-md-12">
										<div class="testimonial">
											<?php the_sub_field( 'content' ); ?>
											<h4><?php the_sub_field( 'testimonial_name' ); ?>, <?php the_sub_field( 'testimonial_location' ); ?></h4>	
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</section>
			<?php } ?>
		<?php elseif ( get_row_layout() == 'treatments_&_prices' ) : ?>
			<?php $background_image = get_sub_field( 'background_image' ); ?>
			<?php if ( $background_image ) : ?>
				<section class="treatments padding" style="background-image: url(<?php echo $background_image['url']; ?>);">
					<div class="container" >
						<div class="grid">
							<div class="treatments-page-wrapper" >
								<div class="col-md-6 col-sm-12">
									<h2><?php the_sub_field("title");?></h2>
									<section class="treatments padding">
										<?php if ( have_rows( 'treatments' ) ) : ?>
											<?php while ( have_rows( 'treatments' ) ) : the_row(); ?>
												<h3><?php the_sub_field( 'treatment_title' ); ?></h3>
												<ul>
													<?php if ( have_rows( 'details' ) ) : ?>
														<?php while ( have_rows( 'details' ) ) : the_row(); ?>
																<li><?php the_sub_field( 'detail_content' ); ?></li>
														<?php endwhile; ?>
													<?php endif; ?>
												</ul>
											<?php endwhile; ?>
										<?php endif; ?>
									</section>
								</div>
								<div class="col-md-2 col-sm-12">
								</div>
								<div class="col-md-4 col-sm-12">
									<section class="contact-form">
										<?php echo do_shortcode('[contact-form-7 id="80" title="Contact form 1"]');?>
									</section>
								</div>
							</div>
						</div>
					</div>
				</section>
			<?php endif; ?>
		<?php elseif ( get_row_layout() == 'page_content' ) : ?>
			<?php $background_image = get_sub_field( 'background_image' ); ?>
			<?php if ( $background_image ) : ?>
				<section class="page-content padding" style="background-image: url(<?php echo $background_image['url']; ?>);">
					<div class="container">
						<h2><?php the_sub_field('title');?></h2>
						<?php the_sub_field( 'content' ); ?>
					</div>
				</section>
			<?php endif; ?>
		<?php elseif ( get_row_layout() == 'contact_us' ) : ?>
			<?php $background_image = get_sub_field( 'background_image' ); ?>
			<?php if ( $background_image ) : ?>
				<section class="contact-us padding" style="background-image: url(<?php echo $background_image['url']; ?>);">
					<div class="container">
						<div class="grid">
							<div class="col-md-6 col-sm-12">
								<section class="contact-details padding">
									<h2><?php the_sub_field("title"); ?></h2>
									<div class="landline">
										<i class="fas fa-phone fa-lg"></i>
										<p><?php the_field( 'landline', 'option' ); ?></p>
									</div>
									<div class="mobile">
										<i class="fas fa-mobile-alt fa-lg"></i>
										<p><?php the_field( 'mobile', 'option' ); ?></p>
									</div>
									<div class="email">
										<i class="fas fa-envelope fa-lg"></i>
										<p><?php the_field( 'email', 'option' ); ?></p>
									</div>
									<div class="addresses">
										<i class="fas fa-home fa-lg"></i>
										<div class="addresses-wrapper">
											<p><?php the_field( 'address_line_1', 'option' ); ?></p>
											<p><?php the_field( 'address_line_2', 'option' ); ?></p>
											<p><?php the_field( 'address_line_3', 'option' ); ?></p>
											<p><?php the_field( 'address_line_4', 'option' ); ?></p>
											<p><?php the_field( 'address_line_5', 'option' ); ?></p>
										</div>
									</div>
								</section>
							</div>
							<div class="col-md-6 col-sm-12">
								<section class="contact-form padding">
									<?php echo do_shortcode('[contact-form-7 id="94" title="Contact form 2"]');?>
								</section>
							</div>
						</div>
					</div>
				</section>
			<?php endif;?>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>


