<?php

/**
 * Change Excerpt Length
 */

function custom_excerpt_length() {
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length');


/**
 * Change Excerpt Ending
 */

function custom_excerpt_more() {
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');